#include <pthread.h>
#include <ncurses.h>
#include <unistd.h>

#define DELAY1 120000
#define DELAY2 50000

int max_y = 0, max_x = 0;

void *ball_one(void *win)
{
	int x = 0, y = 0;
	int next_x = 0;
	int direction = 1;
	
	while(1) {
		wclear(win);
		
		wmove(win, y, x);
		wprintw(win, "o");
		wrefresh(win);

		usleep(DELAY1);

		next_x = x + direction;

		if (next_x >= max_x || next_x < 0) {
			direction*= -1;
		} else {
			x += direction;
		}
	}

	pthread_exit(NULL);
}

void *ball_two(void *win)
{
	int x = 0, y = 0;
	int next_y = 0;
	int direction = 1;
	
	while(1) {
		wclear(win);
		
		wmove(win, y, x);
		wprintw(win, "X");
		wrefresh(win);

		usleep(DELAY2);

		next_y = y + direction;

		if (next_y >= max_y || next_y < 0) {
			direction*= -1;
		} else {
			y += direction;
		}
	}

	pthread_exit(NULL);
}

int main(int argc, char *argv[]) 
{
	pthread_t thread_one, thread_two;
	void *status_one, *status_two;

	WINDOW *win1, *win2;

	initscr();
	noecho();
	curs_set(FALSE);

	getmaxyx(stdscr, max_y, max_x);
	win1 = newwin(max_y, max_x, 0, 0);
	win2 = newwin(max_y, max_x, 1, 0);

	pthread_create(&thread_one, NULL, ball_one, (void *) win1);
	pthread_create(&thread_two, NULL, ball_two, (void *) win2);

	pthread_join(thread_one, &status_one);
	pthread_join(thread_two, &status_two);

	endwin();
	pthread_exit(NULL);
}
